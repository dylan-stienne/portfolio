import HomePage from "./pages/Home"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="*"><Redirect to="/" /></Route>
      </Switch>
    </Router>
  )
}

export default App;
